import sqlite3


class Base:
    def __init__(self, url):

        # 连接数据库
        self.conn = sqlite3.connect(url)

        # 创建游标
        self.cur = self.conn.cursor()

    # 创建表
    def createTable(self, name, example):
        create_tb = "CREATE TABLE {0} ({1})".format(name, example)
        self.cur.execute(create_tb)
        self.conn.commit()

    # 向表中添加元素
    def insertTo(self, name, example):
        insert_to = "INSERT INTO {0} VALUES ({1})".format(name, example)
        self.cur.execute(insert_to)
        self.conn.commit()

    # 从表中删除元素
    def deleteFrom(self, name, example):
        delete_from = "DELETE from {0} where {1}".format(name, example)
        self.cur.execute(delete_from)
        self.conn.commit()

    # 从表中查找所有元素并返回
    def findAll(self, name):
        self.cur.execute('select * from {0}'.format(name))
        return self.cur.fetchall()

    # 删除表
    def deleteTable(self, name):
        self.cur.execute('drop table %s' % name)
        self.conn.commit()

    # 更新元素
    def updateTo(self, name, who, example):
        updata_to = "UPDATE {0} SET {1} WHERE {2}".format(name, example, who)
        self.cur.execute(updata_to)
        self.conn.commit()
